/**
 * Lib import
 */
const Discord = require('discord.js')
const Parser = require('./parser.js')
const { Commands } = require('./Commands.js')

const client = new Discord.Client() // Init discord client
let commandExec = null

require('dotenv').config() // Init the .env config file

client.on('ready', () => {
  console.log('Bot ready')
  /* Create all commands in the memory */
  commandExec = new Parser.CommandExec([
    { name: 'play', params: true, exec: Commands.play },
    { name: 'stop', params: false, exec: Commands.stop },
    { name: 'create', params: true, exec: Commands.create },
    { name: 'add', params: true, exec: Commands.add },
    { name: 'remove', params: true, exec: Commands.remove },
    { name: 'help', params: false, exec: Commands.help },
    { name: 'info', params: 'both', exec: Commands.info },
    { name: 'vol', params: true, exec: Commands.vol }
    // { name: 'next', params: false, exec: Commands.next },
    // { name: 'previous', params: false, exec: Commands.previous }
  ])
})

client.on('message', async message => {
  commandExec.executeCommand(message) // Parse and execute the command
})

client.login(process.env.TOKEN) // Get the token in the environment file

// Listen the port 3000
// require('http')
//   .createServer()
//   .listen(3000, '0.0.0.0', () => {
//     console.log('Listening port 3000')
//   })
