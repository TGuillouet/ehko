/**
 * Global variables declaration
 */
let streamOptions = {
  seek: 0,
  volume: 0.5
}

let messageColor = [255, 0, 0]

let playlists = [
  {
    name: 'test',
    description: '',
    playingIndex: 0,
    list: [
      'https://www.youtube.com/watch?v=yFKCVReyeK8',
      'https://www.youtube.com/watch?v=nAyiH5Bqa5o'
    ]
  },
  {
    name: 'test2',
    description: 'Ma super playlist',
    playingIndex: 0,
    list: [
      'https://www.youtube.com/watch?v=nAyiH5Bqa5o',
      'https://www.youtube.com/watch?v=yFKCVReyeK8'
    ]
  }
]
let selectedPlaylist = playlists[0]

/* Exporting all variables */
module.exports = {
  streamOptions,
  playlists: {
    get: () => playlists,
    set: newList => {
      playlists = newList
    },
    add: playlist => {
      playlists.push(playlist)
    }
  },
  selectedPlaylist: {
    get: () => selectedPlaylist,
    set: playlist => {
      selectedPlaylist = playlist
    }
  },
  messageColor
}
