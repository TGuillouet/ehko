/**
 * Import modules
 */
let {
  playlists,
  selectedPlaylist,
  messageColor,
  streamOptions
} = require('./Global.js') // Global variables
const Discord = require('discord.js')
let { play: playPlaylist, dispatcher, playNext } = require('./player.js')
const { getInfo } = require('ytdl-core')

/**
 * Display all commands help
 * @param {Message} message
 */
const help = message => {
  let embed = new Discord.RichEmbed()
    .setTitle('Available commands')
    .setDescription('You can use the following commands')
    .setColor(messageColor)
    .addField(
      '*play youtube_url',
      'Play the audio stream of a youtube video',
      true
    )
    .addField(
      '*stop',
      'Stop the current playlist and disconnect the bot from the channel',
      true
    )
    .addField('*create playlist_name', 'Create an empty playlist', true)
    .addField(
      '*add playlist_name youtube_url',
      'Add an audio clip the the specified playlist',
      true
    )
    .addField(
      '*remove playlist_name index',
      'Remove in the specified playlist the audio clip at the specified index',
      true
    )
    .addField('*vol [0-100]', 'Modify the global bot volume (in %)', true)
    .addField(
      '*info (playlist_name)',
      'If a parameter is specified, display the list of audio clips in the playlist else display the list of all playlists',
      true
    )
  message.channel.send({ embed })
}

/**
 * Play a playlist or an url
 * @param {Message} message
 */
const playCmd = message => {
  let param = message.content
    .trim()
    .substr(message.content.trim().indexOf(' ') + 1)
  if (isYtbUrl(param) !== null) {
    let content =
      selectedPlaylist.get().name.length == 0
        ? [...selectedPlaylist.get().list, param]
        : [param]
    selectedPlaylist.set({
      name: '',
      description: '',
      playingIndex: 0,
      list: content
    })
  } else {
    let playlist = getPlayList(param)
    if (playlist.length === 1) {
      selectedPlaylist.set(playlist[0])
    } else {
      message.reply('Please provide a valid youtube url or playlist name')
      return
    }
  }
  if (message.member.voiceChannel.connection != null) return
  message.member.voiceChannel.join().then(connection => {
    playPlaylist(connection, message.channel)
  })
}

/**
 * Disconnect the bot from the voice channel
 * @param {Message} message
 */
const stop = message => {
  if (message.member.voiceChannel === undefined) {
    message.reply('You need to be in the same channel as me')
    return
  }
  message.member.voiceChannel.leave()
}

/**
 * Create a playlist
 * @param {Message} message
 */
const create = message => {
  let playlistInfos = message.content.trim().split(/\s/g)[1]
  playlists.add({
    name: playlistInfos[0],
    description: playlistInfos[1] != undefined ? playlistInfos[1] : '',
    list: []
  })
  let embed = new Discord.RichEmbed()
    .setTitle(`Playlist ${playlistName} created`)
    .setColor(messageColor)
  message.channel.send({ embed })
}

/**
 * Add an audio clip to the
 * @param {Message} message
 */
const add = message => {
  let params =
    message.content
      .trim()
      .match(
        /\*[a-zA-Z]+\s[a-zA-Z1-9]+\shttps:\/\/www\.youtube\.com\/watch\?v=[a-zA-Z0-9]{11}/g
      ) !== null
      ? message.content.trim().split(/\s/g)
      : null
  if (
    params !== null &&
    isYtbUrl(params[2]) &&
    getPlayList(params[1]).length === 1
  ) {
    playlists.set(
      playlists.get().map(i => {
        if (i.name == params[1]) {
          i.list.push(params[2])
          getInfo(params[2], (err, infos) => {
            let embed = new Discord.RichEmbed()
              .setTitle(`${infos.title} is added to the playlist ${i.name}`)
              .setColor(messageColor)
              .setURL(params[2])
            message.channel.send({ embed })
          })
        }
        return i
      })
    )
  } else {
    let embed = new Discord.RichEmbed()
      .setTitle(`The audio clip cannot be added`)
      .setColor(messageColor)
    message.channel.send({ embed })
  }
}

/**
 * Remove the link at the specified index
 * @param {Message} message
 */
const remove = message => {
  let commandArr = message.content
    .trim()
    .split(/\s/g)
    .splice(1)
  if (commandArr.length === 1 && !isNaN(commandArr[0])) {
    // Delete the playlist at the index commandArr[0]
    playlists.set(
      playlists.get().filter((playlist, index) => {
        if (index != commandArr[0] - 1) return playlist
      })
    )
  } else if (commandArr.length == 2 && !isNaN(commandArr[1])) {
    let requestedPlaylist = getPlayList(commandArr[0])[0] // Get the playlist

    /* Modify the playlist (is modified globally too) */
    requestedPlaylist.list = requestedPlaylist.list.filter(
      (audioClip, index) => {
        if (index != commandArr[1] - 1) return audioClip
      }
    )
  } else {
    message.reply('Invalid parameters')
  }
}

/**
 * Get the playlists list of the infos of the specified playlist
 * @param {Message} message
 */
const info = async message => {
  let commandArr = message.content
    .trim()
    .trim()
    .split(/\s/g)
    .splice(1)
  let embed = new Discord.RichEmbed().setColor(messageColor)
  if (commandArr.length === 0) {
    // if the command has no parameters
    embed.setTitle(`All playlists:`)
    playlists.get().forEach((element, index) => {
      embed.addField(
        `${index + 1} | ${element.name}`,
        element.description != '' ? element.description : 'No description',
        false
      )
      // embed.
    })
  } else if (commandArr.length === 1) {
    embed.setTitle(`Playlist ${commandArr[0]} infos:`)
    let playList = getPlayList(commandArr[0])[0]
    for (let i = 0; i < playList.list.length; i++) {
      let video_infos = await getInfo(playList.list[i])
      embed.addField(
        `${i + 1} | ${video_infos.title}`,
        `${playList.list[i]}`,
        true
      )
    }
  } else {
    embed.setTitle(
      'Please enter a valid playlist id, see help for more informations'
    )
  }
  message.channel.send({ embed })
}

/**
 * Go to the previous song
 * @param {Message} message
 */
const previous = message => {
  // selectedPlaylist.playingIndex -= 1
}

/**
 * Go to the next song
 * @param {Message} message
 */
const next = message => {
  // End the dispatcher (dispatcher var is needed)
  // Increment the playingIndex
  // Play the clip (message.member.voiceChannel.connection normalement) (connection)
}

/**
 * Modify the volume for the next audio clip
 * @param {Message} message
 */
const vol = message => {
  let commandArr = message.content
    .trim()
    .split(/\s/g)
    .splice(1)
  if (!isNaN(commandArr[0]) && commandArr[0] >= 0 && commandArr[0] <= 100) {
    streamOptions.volume = parseInt(commandArr[0]) / 100
  } else {
    message.reply('Invalid parameter')
  }
}

/**
 * ===================================================
 * Player
 * ===================================================
 */

/**
 * ===================================================
 * Reusable logic
 * ===================================================
 */

/**
 * Check if the url is a youtube url
 * @param {String} url
 */
let isYtbUrl = url => {
  return url.match(/https:\/\/www\.youtube\.com\/watch\?v=[a-zA-Z0-9]{11}/g)
}

/**
 * Search playlist is the global playlists list
 * @param {String} name
 */
let getPlayList = name => {
  return playlists.get().filter(p => name === p.name)
}

/* Export functions */
exports.Commands = {
  play: playCmd,
  help,
  stop,
  create,
  add,
  remove,
  info,
  next,
  previous,
  vol
}
