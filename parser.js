/**
 * The class who will parse and execute a command
 */
class CommandExec {
  constructor (commandList) {
    this.prefix = '*' // Define the prefix
    this.commandList = commandList // Get all commands
  }

  /**
   * Check if the command exists and execute her if she exists
   * @param {Message} message The message who will be parsed
   */
  executeCommand (message) {
    this.commandList.forEach(command => {
      if (message.content.match(this.getRegex(command))) {
        command.exec(message)
      }
    })
  }

  /**
   * The function who will return the regex
   * @param {Object} command The command
   */
  getRegex (command) {
    let suffix = ''
    if (command.params == true) {
      suffix = '\\s[a-zA-Z0-9\\s]+'
    } else if (command.params == 'both') {
      suffix = '[a-zA-Z0-9\\s\\:\\/\\.\\?\\=]{0,}'
    }
    return `${this.escape(this.prefix)}${command.name}` + suffix
  }

  /**
   * Escape a character
   * @param {Char} charToEscape
   */
  escape (charToEscape) {
    return `\\${charToEscape}`
  }
}

exports.CommandExec = CommandExec
