/**
 * Import modules
 */
let { streamOptions, selectedPlaylist, messageColor } = require('./Global.js') // Global variables
const RichEmbed = require('discord.js').RichEmbed
const ytdl = require('ytdl-core')

let dispatcher = null

/**
 * Play the video audio from the url
 * @param {VoiceConnection} connection
 * @param {String} url
 */
const play = (connection, channel) => {
  let url = selectedPlaylist.get().list[selectedPlaylist.get().playingIndex] // Get the current url
  const stream = ytdl(url, { filters: 'audioonly' }) // Init the stream
  // console.log(stream)
  if (stream && connection && url !== '') {
    /* Get the infos of the stream and display them in an embed message */
    ytdl.getInfo(url, (err, infos) => {
      if (infos.video_url === url) {
        let embed = new RichEmbed()
          .setTitle('Currently playing: ' + infos.title)
          .setDescription(infos.video_url)
          .setColor(messageColor)
          .setAuthor(infos.author.name)
          .setURL(url)
        channel.send({ embed })
      }
    })

    dispatcher = connection.playStream(stream, streamOptions) // Play the stream
    // console.log(dispatcher)
    /**
     * Events listeners
     */
    dispatcher.on('end', r => {
      // dispatcher = null
      console.log('end ' + r)
      playNext(connection, channel) // Play the next element in the playlist
    })
    dispatcher.on('error', e => console.log(e))
  }
}

/**
 * Play the next song in the selected playlist
 * @param {VoiceConnection} connection
 */
function playNext (connection, channel) {
  selectedPlaylist.get().playingIndex++
  // console.log(selectedPlaylist.get())
  console.log(selectedPlaylist.get().list.length)
  console.log(selectedPlaylist.get().playingIndex)
  if (
    selectedPlaylist.get().list.length > selectedPlaylist.get().playingIndex
  ) {
    play(connection, channel) // Play the next audio clip
  } else {
    console.log('disconnect')
    connection.disconnect()
  }
}

exports.play = play
exports.playNext = playNext
exports.dispatcher = dispatcher
